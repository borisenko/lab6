#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "bmp_struct.h"
enum stat {
	STAT_OK=0,
	STAT_ERR_OPEN,
	STAT_ERR_CLOSE
};
typedef enum stat stat;
static const char* io_message[]={
	"Успешное завершение действия над файлом",
	"Ошибка при открытии файла",
	"Ошибка при закрытии файла"
};

int main(int args,char* argv[]){
	FILE *in,*out;
	image* image;
	const char* message;
	read_status read_status;
	write_status write_status;
	if (args==2) {
		puts("Необходимо ввести название файла и угол при старте программы");
		return 0;
	}
	in=fopen(argv[1],"rb");
	if (in==NULL) {
		printf("%s\n",io_message[STAT_ERR_OPEN]);
		return 0;
	}else{
		printf("%s\n",io_message[STAT_OK]);
		image=malloc(sizeof(image));
		read_status=from_bmp(in,image);
		message=get_message_read(read_status);
		printf("%s\n",message);
		if (!strcmp(message,"Изображение прочитано")) {
			image=rotate(image,atoi(argv[2]));
			fclose(in);
			out=fopen(argv[1],"wb");
			write_status=to_bmp(out,image);
			printf("%s\n",get_message_write(write_status));
		}
		free(image);
	}
	if (!fclose(in)) {
		printf("%s\n",io_message[STAT_OK]);
	}else printf("%s\n",io_message[STAT_ERR_CLOSE]); 
	return 0;
}