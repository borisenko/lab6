all: main

main:  bmp_struct.c main.c
	gcc -std=c99 -ansi -pedantic-errors -Wall -Werror bmp_struct.c main.c -o main  -lm

clean:
	rm -f main