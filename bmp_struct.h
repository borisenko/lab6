#ifndef _BMP_STRUCT_H_
#define _BMP_STRUCT_H_
#include <stdio.h>
struct __attribute__((packed)) bmp_header;
struct __attribute__((packed))pixel;
struct image;

enum read_status{
	READ_OK = 0,
	READ_HEADER_ERROR,
	READ_BAD_FILE,
	READ_BAD_BITS
};

enum write_status{
	WRITE_OK = 0,
	WRITE_BAD_HEADER,
	WRITE_BAD_BITS
};

typedef struct bmp_header bmp_header;
typedef struct image image;
typedef struct pixel pixel;
typedef enum read_status read_status;
typedef enum write_status write_status;
const char* get_message_read(read_status status); 
const char* get_message_write(write_status status);
image* rotate(image* const source,int angle);
read_status from_bmp(FILE* in,image* const img);
write_status to_bmp(FILE* out,image* const img);
#endif