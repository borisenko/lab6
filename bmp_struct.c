#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include "bmp_struct.h"

#define BF_TYPE 19778
#define BF_RESERVED 0
#define B_OFF_BITS 54
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_BIT_COUNT 24
#define BI_COMPRESSION 0
#define BI_SIZE_IMAGE 0
#define BI_X_PELS_PER_METER 0
#define BI_Y_PELS_PER_METER 0
#define BI_CLR_IMPORTANT 0
#define BI_CLR_USED 0
#define M_PI 3.14159

double round(double);

struct __attribute__((packed)) bmp_header{
	uint16_t bfType;
	uint32_t bfileSize;
	uint32_t bfReserved;
	uint32_t bOffBits;
	uint32_t biSize;
	uint32_t biWidth;
	uint32_t biHeight;
	uint16_t biPlanes;
	uint16_t biBitCount;
	uint32_t biCompression;
	uint32_t biSizeImage;
	uint32_t biXPelsPerMeter;
	uint32_t biYPelsPerMeter;
	uint32_t biClrUsed;
	uint32_t biClrImportant;
};

struct __attribute__((packed))pixel{
	uint8_t b,g,r;
};

struct image{
	uint32_t width, height;
	pixel* data;	
};

static const char* read_message[]={
	"Изображение прочитано",
	"Невозможно прочитать заголовок файла",
	"Недопустимый формат файла",
	"Ошибка во время чтения битов"
};

static const char* write_message[]={
	"Изображение сохранено",
	"Ошибка при записи заголовка",
	"Ошибка во время сохранения"
};

const char* get_message_read(read_status status){
	return read_message[status];
} 

const char* get_message_write(write_status status){
	return write_message[status];
} 

static int32_t turn_x(double angle, int32_t old_x, int32_t old_y) {
	return round(old_x*cos(angle)+old_y*sin(angle));
}
static int32_t turn_y(double angle, int32_t old_x, int32_t old_y) {
	return round(-old_x*sin(angle)+old_y*cos(angle));
}


image* rotate(image* const source,int angle){
	struct pixel* old_pixels=source->data;
	if (angle%90!=0){
		puts("Можно вводить только углы кратные 90 градусам");
		return source;
	}
	if ((angle/90)%2==1){
		uint32_t width=source->width;
		uint32_t height=source->height;
		uint32_t x_center=width/2;
		uint32_t y_center=height/2;
		struct pixel* new_pixels = malloc(width*height*sizeof(pixel));
		size_t i,j;
	
		for (i=0;i<height;i++) {
			for (j=0;j<width;j++) {
				int32_t old_coordinate_x=j-x_center;
				int32_t old_coordinate_y=i-y_center;
				uint32_t new_coordinate_x=y_center+turn_x(angle*M_PI/180,old_coordinate_x,old_coordinate_y);
				uint32_t new_coordinate_y=x_center+turn_y(angle*M_PI/180,old_coordinate_x,old_coordinate_y);
				*(new_pixels+new_coordinate_y*height+new_coordinate_x)=*(old_pixels+i*width+j);
			}
		}
		source->width=height;
		source->height=width;
		source->data=new_pixels;
		free(old_pixels);
		return source;
	}else{
		uint32_t width=source->width;
	    uint32_t height=source->height;
        size_t i,j;
        struct pixel* new_pixels=malloc(width*height*sizeof(pixel));
		for(i=0; i < height; i++){
			for(j=0; j < width; j++){
				*(new_pixels+(height-i-1)*width+(width-j-1))=*(old_pixels+i*width+j);
			}
		}
		source->data=new_pixels;
		free(old_pixels);
		return source;
	}
}

read_status from_bmp(FILE* in,image* const img){
	bmp_header* header=malloc(sizeof(bmp_header));
	uint8_t rubbish[4],rubbish_bits;
	uint32_t biWidth,biHeight,i;
	uint64_t size_row,image_bits;
	pixel* files_pixel;
	if (fread(header,sizeof(bmp_header),1,in)!=1)
		return READ_HEADER_ERROR;
	fseek(in,header->bOffBits,SEEK_SET);
	if (header->biBitCount!=BI_BIT_COUNT) return READ_BAD_FILE;
	if (header->bfType!=BF_TYPE) return READ_BAD_FILE;
	biWidth=header->biWidth;
	biHeight=header->biHeight;
	size_row=biWidth*sizeof(pixel);
	rubbish_bits=size_row%4==0?0:4-size_row%4;
	image_bits=sizeof(pixel)*biHeight*biWidth;
	files_pixel=malloc(image_bits);
	for (i=0;i<biHeight;i++){
		if(fread(files_pixel+biWidth*i,size_row,1,in)!=1) return READ_BAD_BITS;
		if (files_pixel!=0) fread(rubbish,rubbish_bits,1,in);
	}
	img->data=files_pixel;
	img->width=biWidth;
	img->height=biHeight;
	printf("Ширина изображения %u, высота=%u\n",img->width,img->height);
	printf("Размер выделенной памяти для пикселей: %lu\n",image_bits);
	free(header);
	return READ_OK;
}

static bmp_header* create_header(image* img){
	bmp_header* header=malloc(sizeof(bmp_header));
	uint32_t height=img->height;
	uint32_t width=img->width;
	uint64_t size_row=width*sizeof(pixel);
	size_row=size_row%4==0?size_row:size_row+4-size_row%4;
	header->bfType=BF_TYPE;
	header->bfileSize=B_OFF_BITS+height*size_row;
	header->bfReserved=BF_RESERVED;
	header->bOffBits=B_OFF_BITS;
	header->biSize=BI_SIZE;
	header->biWidth=width;
	header->biHeight=height;
	header->biPlanes=BI_PLANES;
	header->biBitCount=BI_BIT_COUNT;
	header->biCompression=BI_COMPRESSION;
	header->biSizeImage=height*size_row;
	header->biXPelsPerMeter=BI_X_PELS_PER_METER;
	header->biYPelsPerMeter=BI_Y_PELS_PER_METER;
	header->biClrUsed=BI_CLR_USED;
	header->biClrImportant=BI_CLR_IMPORTANT;
	return header;
}

write_status to_bmp(FILE* out,image* const img){
	bmp_header* header=create_header(img);
	pixel* new_pixels=img->data;
	uint32_t width=img->width;
	uint32_t height=img->height;
	uint8_t rubbish_bits=width*sizeof(pixel)%4==0?0:4-width*sizeof(pixel)%4;
	uint8_t rubbish[4] = {0};
	uint64_t i;
	if (fwrite(header,sizeof(bmp_header),1,out)!=1) return WRITE_BAD_HEADER;
	for (i=0;i<height;i++) {
		if (fwrite(new_pixels+i*width,sizeof(pixel),width,out)!=width) return WRITE_BAD_BITS;
		if (rubbish_bits!=0) fwrite(&rubbish,sizeof(uint8_t),rubbish_bits,out);
	}
	free(header);
	free(img->data);
	return WRITE_OK;
}